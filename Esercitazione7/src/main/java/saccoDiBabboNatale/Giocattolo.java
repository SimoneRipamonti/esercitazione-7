package saccoDiBabboNatale;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Giocattolo {
	private final int sn;
	private static int snCounter = 0;
	private static int nDinosauriLeft = 20;
	private static int nDraghettiLeft = 20;
	private static int nPinguiniLeft = 20;

	protected Giocattolo(int sn) {
		this.sn = sn;
	}

	public int getSn() {
		return sn;
	}

	public static Giocattolo createGiocattolo()
			throws GiocattoliFinitiException {
		if (nDinosauriLeft == 0 && nPinguiniLeft == 0 && nDraghettiLeft == 0) {
			throw new GiocattoliFinitiException(
					"Rivolgiti ad un altro Babbo Natale");
		}

		// aggiungo i giocattoli alla lista
		
		List<Giocattolo> giocattoli = new ArrayList<Giocattolo>();
		if (nDinosauriLeft > 0) {
			giocattoli.add(new Dinosauro(snCounter + 1));
		}
		if (nPinguiniLeft > 0) {
			giocattoli.add(new Pinguino(snCounter + 1));
		}
		if (nDraghettiLeft > 0) {
			giocattoli.add(new Draghetto(snCounter + 1));
		}
		
		// estraggo un giocattolo
		
		Collections.shuffle(giocattoli);
		Giocattolo daRitornare = giocattoli.get(0);
		
		// modifico i contatori
		
		snCounter++;
		if (daRitornare instanceof Dinosauro) {
			nDinosauriLeft--;
		}
		if (daRitornare instanceof Pinguino) {
			nPinguiniLeft--;
		}
		if (daRitornare instanceof Draghetto) {
			nDraghettiLeft--;
		}
		return daRitornare;
	}
}
