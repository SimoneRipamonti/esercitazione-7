package singleton;

public class Main {

	public static void main(String[] args) {
		CreatoreDiLabirinti creatore = CreatoreDiLabirinti.getInstance();
		System.out.println(creatore);
		CreatoreDiLabirinti creatore2 = CreatoreDiLabirinti.getInstance();
		System.out.println(creatore2);
		Labirinto lab1 = creatore.creaLabirinto();
	}

}
