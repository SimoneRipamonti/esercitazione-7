package singleton;

//public class CreatoreDiLabirinti {
//
//	// in questa classe applico il design pattern singleton, implementazione
//	// pigra perch� CreatoreDiLabirinti viene creato quando getInstance() viene
//	// invocata per la prima volta, vantaggi dal punto di vista della memoria
//	// allocata, alloco solo quando mi serve;
//
//	// PROBLEMA: non � safe in contesto multithreading con pi� thread in
//	// parallelo, pu� accadere che il thread 1 verifica che l'instanza � null,
//	// tocca al thread 2 che verifica anchesso che � null e crea un
//	// CreatoreDiLabirinti, torna a thread 1 e anche lui crea un nuovo
//	// CreatoreDiLabirinti. Ho 2 CreatoriDiLabirinti quando il pattern prevedeva
//	// ce ne fosse solo 1
//
//	private static CreatoreDiLabirinti instance = new CreatoreDiLabirinti();
//
//	// SOLUZIONE 2: sposto la creazione di CreatoreDiLabirinti al momento
//	// dell'instanziazione
//
//	private CreatoreDiLabirinti() {
//		// costruttore private, non accessibile dall'esterno
//	}
//
//	public static synchronized CreatoreDiLabirinti getInstance() {
//		// SOLUZIONE 1: synchronized indica che il metodo pu� essere invocato da
//		// un thread alla volta
//		// se c'� un'instanza la ritorno, altrimenti la creo e la ritorno
//
//		if (instance == null) {
//			instance = new CreatoreDiLabirinti();
//		}
//
//		return instance;
//	}
//
//	public Labirinto creaLabirinto() {
//		return new Labirinto();
//	}
//}

public enum CreatoreDiLabirinti {
	INSTANCE;
	private CreatoreDiLabirinti() {

	}

	public Labirinto creaLabirinto() {
		return new Labirinto();
	}

	public static CreatoreDiLabirinti getInstance() {
		return INSTANCE;
	}
}
